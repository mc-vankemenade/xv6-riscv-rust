#!/bin/bash
set -e

rm -rf target/
cargo rustc -- -C link-arg=--script=./linker.ld --target=riscv64gc-unknown-none-elf
riscv64-linux-gnu-objcopy -O binary target/debug/xv6-riscv-rust ./kernel.img

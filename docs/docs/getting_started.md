## Setting up the Risc-v toolchain
There are a number of tools required to build and run the xv6 operating system. The GNU riscv toolchain is used for debugging and linking the project.
Qemu is used for running a virtual machine containing the operating system.

### The GNU Riscv Toolchain
To get the riscv Toolchain up and running install the tools from the riscv-gnu-toolchain repo.

- `https://github.com/riscv-collab/riscv-gnu-toolchain.git`

### Qemu
In order to emulate the riscv machine, Install the following packages:

- `qemu`
- `qemu-system-riscv64`

### Other tools
- `Rust (installed using rustup)`
- `ld`
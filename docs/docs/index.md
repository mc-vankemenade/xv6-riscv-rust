## XV6 in Rust
This project aims to reimplement the XV6 operating system using Rust. XV6 in itself is an open-source implementation of the UNIX 6 kernel written in ansi-C.

The main goal of this operating system is to explore the inner workings of the riscv ISA and to get more familiar with baremetal programming, and the levels of abstraction that are created when building a kernel.
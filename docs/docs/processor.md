# Risc-V Hart internals
## Introduction
Risc-V is an open source instruction set. Meaning anyone can use the standards and architecture defined in the ISA to produce their own processors. This enables chip manufacturers to produce these CPU's for lower prices. While also enabling developers to more easily learn the inner workings of the processors and quickly jump into developing software for the platform.

---

## Terminology
### Harts
Risc-v uses the term __Harts__ to refer to each core within one CPU. Each hart operates independently from one-another. Though they do have a unique ID, which can be read from the `mhartid` register.

### registers
A Risc-V processor contains 32 registers that can be used by software. These registers can vary in width depending on the Risc-V architecture that is used. xv6 uses the 64 bit version of Risc-V. 

Information about the exact registers can be found here on [WikiChip](https://en.wikichip.org/wiki/risc-v/registers).

### CSR
CSR stands for Control & Status Register. These special registers are used to set up interrups, jump between privilege levels, set up timers, and handle exceptions. Each privilege level has its own set of instructions and registers. However each privilege level can also access the registers of the levels below it.

### Privilege levels
Risc-V operates on a set of privilege levels that the software can switch between. This protects "higher level" pieces of software from interacting with physical memory or causing exceptions. 
![Privilege Levels](img/privilege_levels.jpg)

---
## Control & Status Register Listing
### Machine-mode CSR's
When machine mode is active, the following CSR's are available:
![Machine CSRs 1](img/machine_csrs.jpg)
![Machine CSRs 2](img/machine_csrs_2.jpg)

### Supervisor-mode CSR's
When in supervisor mode, the following CSR's are available:
![Supervisor CSRs](img/supervisor_csr.jpg)

### User-mode CSR's
When in user mode, the following CSR's are available:
![User CSRs](img/unprivileged_csrs.jpg)

---
## Machine Mode registers
### mhartid
the mhartid register holds the unique id for the specific hart. This is used multiple times to differentiate each hart. And for example, initiate the stack-pointer of each hart at a different location.

### mstatus
The mstatus register holds a multitude of different parameters that can be set in machine mode. Where each bit in the 64-bit register is tied to a different function. The layout looks as follows:
![mstatus layout](img/mstatus_layout.jpg)

There are a number of relevant sections within the mstatus register:

- `SIE` Supervisor Interrupt Enable
    - Setting this bit enables interrupts in supervisor mode.
- `MIE` Machine Interrupt Enable
    - Setting this bit enables interrupts in machine mode.
- `MPP` Machine Previous Privilege
    - These 2 bits set the privilege mode the processor should return to whenever the `mret` instruction is called.
- `SPP` Supervisor Previous Privilege
    - This 1 bit sets the privilege mode the processor should return to whenever the `sret` instruction is called.
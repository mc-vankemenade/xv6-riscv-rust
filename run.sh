#!/bin/bash
set -e

qemu-system-riscv64 -machine virt -bios none -kernel ./kernel.img -m 128M -smp 3 -nographic -global virtio-mmio.force-legacy=false -s -S
    #-device virtio-blk-device,drive=x0,bus=virtio-mmio-bus.0
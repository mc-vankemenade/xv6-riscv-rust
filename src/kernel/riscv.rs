pub mod misc_registers;
pub mod control_registers;

pub use control_registers::*;
pub use misc_registers::*;
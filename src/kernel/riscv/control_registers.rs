use core::arch::asm;

pub fn read_mhartid() -> u64 {
    let mut i: u64;
    unsafe {
        asm!(
            "csrr {i}, mhartid",
            i = out(reg) i
        );
    }
    return i
}

// mask stripping the MSTATUS register to the MPP value which is bit 11 and 12.
pub const MSTATUS_MPP_MASK: u64 = 0b11 << 11;

// The different modes the MPP bits can be set to.
pub const MSTATUS_MPP_M: u64 = 0b11 << 11; // Machine mode
pub const MSTATUS_MPP_S: u64 = 0b01 << 11; // Supervisor mode
pub const MSTATUS_MPP_U: u64 = 0b00 << 11; // User mode

pub fn read_mstatus() -> u64 {
    let mut i: u64;
    unsafe {
        asm!(
            "csrr {i}, mstatus",
            i = out(reg) i
        );
    }
    return i
}

pub fn write_mstatus(i: u64){
    unsafe {
        asm!(
            "csrw mstatus, {i}",
            i = in(reg) i
        );
    }
}

pub fn read_mepc() -> u64 {
    let mut i: u64;
    unsafe {
        asm!(
            "csrr {i}, mepc",
            i = out(reg) i
        );
    }
    return i
}

pub fn write_mepc(i: fn() -> !){
    unsafe {
        asm!(
            "csrw mepc, {i}",
            i = in(reg) i
        );
    }
}
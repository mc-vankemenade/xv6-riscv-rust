use core::arch::asm;

pub fn read_t0() -> u64 {
    let mut i: u64;
    unsafe {
        asm!(
            "mv {i}, t0",
            i = out(reg) i,
        );
    }
    return i;
}

pub fn write_t0(i: u64) {
    unsafe {
        asm!(
            "mv t0, {i}",
            i = in(reg) i,
        );
    }
}

pub fn read_t1() -> u64 {
    let mut i: u64;
    unsafe {
        asm!(
            "mv {i}, t1",
            i = out(reg) i,
        );
    }
    return i;
}

pub fn write_t1(i: u64) {
    unsafe {
        asm!(
            "mv t1, {i}",
            i = in(reg) i,
        );
    }
}

pub fn read_t2() -> u64 {
    let mut i: u64;
    unsafe {
        asm!(
            "mv {i}, t2",
            i = out(reg) i,
        );
    }
    return i;
}

pub fn write_t2(i: u64) {
    unsafe {
        asm!(
            "mv t2, {i}",
            i = in(reg) i,
        );
    }
}
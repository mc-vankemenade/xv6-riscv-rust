use super::riscv::*;
// use super::params::*;
use super::main::*;

use core::panic::PanicInfo;
mod boot {
    use core::arch::global_asm;
    use super::super::params::NCPU;
    
    // Define a 4096 address long stack for each hart.
    static STACK0: [u64; 4096 * NCPU] = [0; 4096 * NCPU];

    // Two dimensional array with five spots for each hart to use as a scratch pad.
    static TIMER_SCRATCH: [[u64; 5]; NCPU] = [[0; 5]; NCPU];
    
    // Pass the stack to the entry.s file and index each hart by their ID.
    global_asm!(include_str!("entry.s"), stack0 = sym STACK0);
}

#[no_mangle]
pub fn start() {

    // Read the mstatus register and set the previous user-level to supervisor
    let mut mstatus: u64 = read_mstatus();
    mstatus &= !MSTATUS_MPP_MASK; // Clear the MPP bits
    mstatus |= MSTATUS_MPP_S;
    write_mstatus(mstatus);

    // Set the Exception Program Counter to point to the main function.
    write_mepc(main);

    // Disable supervisor memory paging

    // funky stuff
    let id: u64 = read_mhartid();
    write_mstatus(id);
    loop {}
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}



